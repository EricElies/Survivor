using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


[RequireComponent(typeof(CharacterController))]

public class PlayerController : MonoBehaviour
{
    public float walkingSpeed = 7.5f;
    public float runningSpeed = 11.5f;
    public float jumpSpeed = 6.0f;
    public float gravity = 20.0f;
    
    public float lookSpeed = 2.0f;
    public float lookXLimit = 45.0f;

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    

    public Animator anim;
    public bool isCrouch;
    public bool canShoot;

    public GameObject mainCamera;
    public GameObject aimCamera;
    public GameObject animCamera;
    public GameObject aimImage;
    public LayerMask aimColliderLayerMask = new LayerMask();



    
    public float rotationPower = 3f;
    public GameObject followTransform;


    public Transform firePoint;
    public Rigidbody bullet;
    public float speedBullet = 300f;
    public int munition;

    public Inventory inventory;

    [HideInInspector]
    public bool canMove = true;
    private bool isAiming;



    void Start()
    {
        characterController = GetComponent<CharacterController>();
        munition = 12;
        isAiming = false;
        inventory = GetComponent<Inventory>();
        // Lock cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        canShoot = true;

    }

    void Update()
    {
        Vector3 mouseWorldPosition = Vector3.zero;

        // We are grounded, so recalculate move direction based on axes
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);
        
        // Press Left Shift to run
        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        float curSpeedX = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Vertical") : 0;
        float curSpeedY = canMove ? (isRunning ? runningSpeed : walkingSpeed) * Input.GetAxis("Horizontal") : 0;
        float movementDirectionY = moveDirection.y;
        moveDirection = (forward * curSpeedX) + (right * curSpeedY);

        


        anim.SetFloat("MoveZ", curSpeedX);
        anim.SetFloat("MoveX", curSpeedY);

        if (Input.GetButton("Jump") && canMove && characterController.isGrounded)
        {
            anim.SetTrigger("Jump");
            moveDirection.y = jumpSpeed;
        }
        else
        {
            moveDirection.y = movementDirectionY;
        }

        if (Input.GetKeyDown(KeyCode.C)) 
        {
            if (isCrouch) 
            {
                anim.SetBool("isCrouch", false);
                isCrouch = false;
                canShoot = true;

            }
            else 
            {
                anim.SetBool("isCrouch", true);
                isCrouch = true;
                moveDirection = (forward * curSpeedX)/2 + (right * curSpeedY)/2;
                canShoot = false;
            }
        }

       

        if (Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(TakeObject());
        }

        IEnumerator TakeObject()
        {
            canMove = false;
            anim.SetTrigger("Using");
            yield return new WaitForSeconds(1f);
            canMove = true;
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        if (!characterController.isGrounded)
        {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        
        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);

        if (canMove)
        {
            transform.rotation *= Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationPower, Vector3.up);

            followTransform.transform.rotation *= Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationPower, Vector3.up);

            followTransform.transform.rotation *= Quaternion.AngleAxis(-Input.GetAxis("Mouse Y") * rotationPower, Vector3.right);

            var angles = followTransform.transform.localEulerAngles;
            angles.z = 0;

            var angle = followTransform.transform.localEulerAngles.x;

            //Clamp the Up/Down rotation
            if (angle > 180 && angle < 340)
            {
                angles.x = 340;
            }
            else if (angle < 180 && angle > 30)
            {
                angles.x = 30;
            }


            followTransform.transform.localEulerAngles = angles;

            transform.rotation = Quaternion.Euler(0, followTransform.transform.rotation.eulerAngles.y, 0);

            //Set the player rotation based on the look transform
            transform.rotation = Quaternion.Euler(0, followTransform.transform.rotation.eulerAngles.y, 0);
            //reset the y rotation of the look transform
            followTransform.transform.localEulerAngles = new Vector3(angles.x, angles.y, 0);
        }

        if (Input.GetButton("Fire1") && canMove && characterController.isGrounded)
        {

            forward = Vector3.zero; right = Vector3.zero;
            anim.SetBool("isShotting", true);
            anim.SetTrigger("Shoot");

        }
        else { anim.SetBool("isShotting", false); } 
         
        
        if (Input.GetButtonDown("Fire2") && canMove && characterController.isGrounded)
        {
            if (!isAiming)
            {
                anim.SetBool("isAiming", true);
                aimImage.SetActive(true);
                aimCamera.SetActive(true);
                mainCamera.SetActive(false);
                isAiming = true;

                Vector3 worldAimTarget = mouseWorldPosition;
                worldAimTarget.y = transform.position.y;
                Vector3 aimDirection = (worldAimTarget - transform.position).normalized;
                transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime *20f);
            }
            else {
                aimCamera.SetActive(false);
                mainCamera.SetActive(true);
                isAiming = false;
                aimImage.SetActive(false);
                anim.SetBool("isAiming", false);
            }
            
        }


        if (Input.GetKey(KeyCode.Tab) && characterController.isGrounded)
        {
            
            StartCoroutine(Celebration());
            
            
        }

        IEnumerator Celebration() 
        {
            canMove = false;
            mainCamera.SetActive(false);
            animCamera.SetActive(true);
            anim.SetBool("isCelebrating", true);
            anim.SetTrigger("Celebrate");
            yield return new WaitForSeconds(4f);
            anim.SetBool("isCelebrating", false);
            animCamera.SetActive(false);
            mainCamera.SetActive(true);
            canMove = true;
        }
    }


    

    public void shoot()
    {
        
        //Vector3 dir = r.GetPoint(2) - r.GetPoint(0);
       //GameObject bullet = Instantiate(shuriken, firePoint.transform.position, Quaternion.LookRotation(dir));
        /*bullet.GetComponent<Rigidbody>().velocity = r.direction * 30;
        bullet.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        */
        Rigidbody instantiatedProjectile = Instantiate(bullet, firePoint.transform.position, firePoint.transform.rotation) as Rigidbody;
        instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0, speedBullet));
        munition--;
    }


   /* private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item") && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("ENTRI");
            GameObject itemPickedUp = other.gameObject;

            ItemPickUp item = itemPickedUp.GetComponent<ItemPickUp>();
            
            
            //inventory.AddItem(itemPickedUp, item.id, item.type, item.description, item.icon);
        }
    }
   */
}