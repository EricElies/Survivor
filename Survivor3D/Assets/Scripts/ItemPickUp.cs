using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : MonoBehaviour
{
    public Item Item;

    void PickUp() 
    {
        InventoryManager.Instance.Add(Item);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            PickUp();
            Debug.Log("Entra");

            //inventory.AddItem(itemPickedUp, item.id, item.type, item.description, item.icon);
        }
    }
}
