using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Item", menuName ="Item/Create New Item")]
public class Item : ScriptableObject
{
    public int id;
    public string type;
    public string description;
    public Sprite icon;
    
    [HideInInspector]
    public bool pickedUp;

    [HideInInspector]
    public bool equipped;

    [HideInInspector]
    public GameObject itemManager;
    [HideInInspector]
    public GameObject item;

    public GameObject nothingToDo;

    public bool playersItem;

    private void Start()
    {
        itemManager = GameObject.FindWithTag("ItemManager");
        if (!itemManager) 
        {
            int allItems = itemManager.transform.childCount;
            for (int i = 0; i < allItems; i++)
            {
                if (itemManager.transform.GetChild(i).gameObject.GetComponent<Item>().id == id) 
                {
                    item = itemManager.transform.GetChild(i).gameObject;
                }
            }

        }
    }

    private void Update()
    {
        if (equipped)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                //UseItem();
                equipped = false;
            }
            
        }
    }

    

    IEnumerator nothingToDoShow() 
    {
        nothingToDo.SetActive(true);
        yield return new WaitForSeconds(1f);
        nothingToDo.SetActive(false);
    }


    public void ItemUsage() 
    {
        //gameObject.GetComponent<Item>().item.SetActive(true);
        item.GetComponent<Item>().equipped = true;
    }
}
