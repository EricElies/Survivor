using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    private bool inventoryEnable;
    public GameObject inventory;
    
    private int allSlots;
    private int enabledSlots;
    private GameObject[] slot;
    public GameObject slotHolder;

    
    
    [HideInInspector]
    private PlayerController playerController;

    void Start()
    {
        
        
        allSlots = slotHolder.transform.childCount;
        playerController = GetComponent<PlayerController>();
        slot = new GameObject[allSlots];

        for (int i = 0; i < allSlots; i++)
        {
            slot[i] = slotHolder.transform.GetChild(i).gameObject;

            if (slot[i].GetComponent<Slot>().item == null)
            {
                slot[i].GetComponent<Slot>().empty = true;
            }
        }

    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I)) 
        {
            inventoryEnable = !inventoryEnable;
        }
        
        if (inventoryEnable) 
        {
            inventory.SetActive(true);
            Cursor.visible = true;
            playerController.canMove = false;
            Cursor.lockState = CursorLockMode.Confined;
        }
        else 
        {
            inventory.SetActive(false);
            Cursor.visible = false;
            playerController.canMove = true;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void OnTriggerStay(Collider other)
    {
       
        if (other.CompareTag("Item") && Input.GetKey(KeyCode.E)) 
        {
            
            ItemPickUp itemPickedUp = other.GetComponent<ItemPickUp>();
            Item item = itemPickedUp.GetComponent<Item>();
            //itemPickedUp.PickUp();

            Debug.Log("Entra");
            //itemPickedUp.instance.PickUp();
            //InventoryManager inventoryManager = GetComponent<InventoryManager>();
            //inventoryManager.Add(item);
            //AddItem(itemPickedUp, item.id, item.type, item.description, item.icon);
        }
    }

    
    public void AddItem(GameObject itemObject, int itemId, string itemType, string itemDescription, Sprite itemIcon) 

    {
        
        for (int i = 0; i < allSlots; i++)
        {
            if (slot[i].GetComponent<Slot>().empty) 
            {
                itemObject.GetComponent<Item>().pickedUp = true;

                slot[i].GetComponent<Slot>().item = itemObject;
                slot[i].GetComponent<Slot>().id = itemId;
                slot[i].GetComponent<Slot>().type = itemType;
                slot[i].GetComponent<Slot>().description = itemDescription;
                slot[i].GetComponent<Slot>().icon = itemIcon;

                itemObject.transform.parent = slot[i].transform;
                itemObject.SetActive(false);

                slot[i].GetComponent<Slot>().UpdateSlot();

                slot[i].GetComponent<Slot>().empty = false;
                return;
            }
            
        }
    }
}